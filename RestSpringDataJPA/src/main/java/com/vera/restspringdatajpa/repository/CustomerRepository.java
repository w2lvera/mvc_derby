package com.vera.restspringdatajpa.repository;

import com.vera.restspringdatajpa.model.Customer;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository<C> extends CrudRepository<Customer, Integer>{
    List<Customer> findByName(String name);
}
