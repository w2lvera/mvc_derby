/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.restspringdatajpa.logical;


import com.vera.restspringdatajpa.model.Customer;
import java.util.ArrayList;
import java.util.List;
import com.vera.restspringdatajpa.sevicies.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Wera
 */
@Component
public class CustomerListProcessorImpl implements CustomerListProcessor {
    @Autowired
     private CustomerService customerService;
     @Override
    public List<Customer> getCustomerList(String prefix) {
        
        final List<Customer> allCustomers = customerService.getAllCustomers();
        final List<Customer> result = new ArrayList<Customer>();
        for (Customer customer : allCustomers) {
            if (customer.getName().startsWith(prefix)) {
                result.add(customer);
            }
        }
        return result;
    }
   

    @Override
    @Autowired
    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Override
    public Customer getCustomerId(int id) {
        return customerService.getById(id);
    }

    

    
   

}
