/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.restspringdatajpa.sevicies;

import com.vera.restspringdatajpa.model.Customer;
import com.vera.restspringdatajpa.repository.CustomerRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Wera
 */
@Service

public class CustomerService {
    @Autowired
	CustomerRepository<Customer> customerRepository;

	@Transactional
	public List<Customer> getAllCustomers() {
		return (List<Customer>) customerRepository.findAll();
	}

	@Transactional
	public List<Customer> findByName(String name) {
		return customerRepository.findByName(name);
	}

	@Transactional
	public Customer getById(Integer id) {
		return customerRepository.findOne(id);
	}

	@Transactional
	public void deleteCustomer(Integer customerId) {
		customerRepository.delete(customerId);
	}

	@Transactional
	public boolean addCustomer(Customer customer) {
		return customerRepository.save(customer) != null;
	}

	@Transactional
	public boolean updateCustomer(Customer customer) {
		return customerRepository.save(customer) != null;
	}
}
