package com.vera.restspringdatajpa.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class AppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
    protected Class<?>[] getRootConfigClasses() {
      //  return new Class[]{DataConfig.class};
      //  return new Class[]{AppConfiguration.class};
         return  new Class[]{WebConfig.class};
    }

    protected Class<?>[] getServletConfigClasses() {
       // return  new Class[]{WebConfig.class};
        return  new Class[0];
    }

    protected String[] getServletMappings() {
        return new String[]{"/"};
    }
}
