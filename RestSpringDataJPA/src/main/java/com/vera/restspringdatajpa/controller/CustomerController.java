package com.vera.restspringdatajpa.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.vera.restspringdatajpa.model.Customer;
import com.vera.restspringdatajpa.sevicies.CustomerService;
import org.springframework.web.bind.annotation.GetMapping;

@RestController
public class CustomerController {

    @Autowired
    CustomerService customerService;

    @RequestMapping("/")
    public String welcome() {//Welcome page, non-rest
        return "Welcome to RestTemplate Example.";
    }

    @RequestMapping(value = "/customer/{id}", method = RequestMethod.GET)
    public @ResponseBody
    Customer getAllCustomer(@PathVariable Integer id) {
        return customerService.getById(id);
    }

    @RequestMapping(value = "/customerByName/{name}", method = RequestMethod.GET)
    public List<Customer> getCustomerByName(@PathVariable String name) {
        return customerService.findByName(name);
    }

    @RequestMapping(value = "/customer", method = RequestMethod.GET)
    public List<Customer> getAll() {
        return customerService.getAllCustomers();
    }

    @RequestMapping(value = "/customer/{id}", method = RequestMethod.DELETE)
    public HttpStatus deleteCustomer(@PathVariable Integer id) {
        customerService.deleteCustomer(id);
        return HttpStatus.NO_CONTENT;
    }

    @RequestMapping(value = "/customer", method = RequestMethod.POST)
    public HttpStatus insertCustomer(@RequestBody Customer customer) {
        return customerService.addCustomer(customer) ? HttpStatus.CREATED : HttpStatus.BAD_REQUEST;
    }

    @RequestMapping(value = "/customer", method = RequestMethod.PUT)
    public HttpStatus updateCustomer(@RequestBody Customer customer) {
        return customerService.updateCustomer(customer) ? HttpStatus.ACCEPTED : HttpStatus.BAD_REQUEST;
    }
}
