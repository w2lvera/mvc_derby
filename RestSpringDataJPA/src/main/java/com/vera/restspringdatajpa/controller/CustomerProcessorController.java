/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.restspringdatajpa.controller;

import com.vera.restspringdatajpa.logical.CustomerListProcessor;
import com.vera.restspringdatajpa.model.Customer;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Wera
 */
@RestController
@RequestMapping("/customerProcessor")
public class CustomerProcessorController {
    @Autowired
	CustomerListProcessor listProcessor;
    @GetMapping ("/list/{prefix}")
    public List<Customer> listCustomer(@PathVariable String prefix){
        List<Customer> customers = listProcessor.getCustomerList(prefix);
        return customers;
    } 
}




