/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.jspspringdatajpa.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import com.vera.jspspringdatajpa.logical.CustomerListProcessor;
import com.vera.jspspringdatajpa.model.Customer;


/**
 *
 * @author Wera
 */
@Controller
@RequestMapping("/customerProcessorView")
public class CustomerProcessorController {
    @Autowired
    CustomerListProcessor listProcessor;
    @GetMapping ("/list/{prefix}")
    public String listCustomer(@PathVariable String prefix, Model model){
        List<Customer> customers = listProcessor.getCustomerList(prefix);
        model.addAttribute("customers",customers);
        return "list-processor";
    } 
}
