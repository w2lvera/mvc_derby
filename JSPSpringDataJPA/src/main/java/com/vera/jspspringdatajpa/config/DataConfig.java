/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.jspspringdatajpa.config;

/**
 *
 * @author Wera
 */


import java.util.Properties;
import javax.sql.DataSource;
import org.hibernate.ejb.HibernatePersistence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ComponentScan(basePackages = {
    "com.vera.jspspringdatajpa.logical", "com.vera.jspspringdatajpa.sevicies"})
@EnableJpaRepositories("com.vera.jspspringdatajpa.repository")
@EnableTransactionManagement
@PropertySource("classpath:database.properties")
public class DataConfig {

    public DataConfig() {
        
    }

    @Autowired
    Environment environment;

    private final String URL = "url";
    private final String USER = "parol";
    private final String DRIVER = "driver";
    private final String PASSWORD = "password";
    private final String PROPERTY_SHOW_SQL = "hibernate.show_sql";
    private final String PROPERTY_DIALECT = "hibernate.dialect";

    @Bean
    DataSource dataSource() {
        DriverManagerDataSource driverManagerDataSource = 
                new DriverManagerDataSource();
        driverManagerDataSource.setUrl(environment.getProperty(URL));
        driverManagerDataSource.setUsername(environment.getProperty(USER));
        driverManagerDataSource.setPassword(environment.getProperty(PASSWORD));
        driverManagerDataSource.setDriverClassName(environment.getProperty(DRIVER));
        return driverManagerDataSource;
    }

    @Bean
    LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean lfb =
                new LocalContainerEntityManagerFactoryBean();
        lfb.setDataSource(dataSource());
        lfb.setPersistenceProviderClass(HibernatePersistence.class);
        lfb.setPackagesToScan("com.vera.jspspringdatajpa.model");
        lfb.setJpaProperties(hibernateProps());
        return lfb;
    }

    @Bean
    Properties hibernateProps() {
        Properties properties = new Properties();
        properties.setProperty
        (PROPERTY_DIALECT, environment.getProperty(PROPERTY_DIALECT));
        properties.setProperty
        (PROPERTY_SHOW_SQL, environment.getProperty(PROPERTY_SHOW_SQL));
        return properties;
    }

    @Bean
    JpaTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = 
                new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(
                entityManagerFactory().getObject());
        return transactionManager;
    }

}
