<%-- 
    Document   : list-procesor
    Created on : 09.04.2019, 19:59:04
    Author     : Wera
--%>

<%@ page language="java" contentType="text/html; charset=windows-1251"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<title>javaguides.net</title>
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">
<script src="<c:url value="/resources/js/jquery-1.11.1.min.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
</head>
<body>
	<div class="container">
		<div class="col-md-offset-1 col-md-10">
			<h2>CRM - Customer Relationship Manager</h2>
			<hr />
                        
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="panel-title">Customer List</div>
				</div>
				<div class="panel-body">
					<table class="table table-striped table-bordered">
						<tr>
							<th>customerId</th>
							<th>customer Name</th>
							<th>addressline1</th>
							<th>addressline2</th>
                                                        <th>city</th>
						</tr>

						<!-- loop over and print our customers -->
						<c:forEach var="tempCustomer" items="${customers}">

							
							<tr>
                                                            
                                                            <td>${tempCustomer.customerId}</td>
								<td>${tempCustomer.name}</td>
								<td>${tempCustomer.addressline1}</td>
								<td>${tempCustomer.addressline2}</td>
                                                                <td>${tempCustomer.city}</td>

								

							</tr>

						</c:forEach>

					</table>

				</div>
			</div>
		</div>

	</div>
</body>

</html>

